variable "vpc_id" {
  description = "The id of the VPC."
}

variable "region" {
  description = "The AWS region to create resources in."
  default     = "eu-west-1"
}

variable "service_name" {
  description = "Service project name"
}

variable "ecs_cluster_name" {
  description = "The name of the Amazon ECS cluster."
}

variable "service1_ecs_log_group" {
  description = "The log group where to store the logs in aws cloudwatch."
}

variable "service2_ecs_log_group" {
  description = "The log group where to store the logs in aws cloudwatch."
}

variable "service1_container_name" {
  description = "The container's name."
}

variable "service2_container_name" {
  description = "The container's name."
}

variable "service1_container_port" {
  description = "Container service exposed port"
}

variable "service2_container_port" {
  description = "Container service exposed port"
}

variable "host_port" {
  description = "Host exposed port. Default to 0 due to dynamic port mapping for ECS-ALB usage."
  default     = 0
}

variable "ecs_desired_count" {
  description = "ECS service desired count"
  default     = 1
}

variable "ecs_deployment_min_healthy_percent" {
  description = "ECS service Deployment Minimum Healthy percent value"
  default     = 75
}

variable "alb_name" {
  description = "Application Load Balancer name"
}

variable "alb_port" {
  description = "Application Load Balancer port"
  default     = 80
}

variable "tg_name" {
  description = "Application Load Balancer name"
}

variable "alb_tg_target_type" {
  description = "Type of target for target group"
  default     = "instance"
}

variable "alb_tg_protocol" {
  description = "Type of target for target group"
  default     = "HTTP"
}

variable "alb_listener_rule_action_type" {
  description = "The type of routing action."
  default     = "forward"
}

variable "healthy_status_code" {
  description = "Expected status code for the healthcheck"
  default     = 200
}

variable "health_check_path" {
  description = "Health check path for ALB TG. i.e.: /status"
}

variable "service1_cpu" {
  description = "The number of cpu units used by the task"
}

variable "service1_memory" {
  description = "The amount (in MiB) of memory used by the task."
}

variable "service2_cpu" {
  description = "The number of cpu units used by the task"
}

variable "service2_memory" {
  description = "The amount (in MiB) of memory used by the task."
}

variable "service1_stream_prefix" {
  description = "The prefix to decorate each line of the logs."
}

variable "service2_stream_prefix" {
  description = "The prefix to decorate each line of the logs."
}

variable "mount_points" {
  description = "The Docker container volumes that need to be mapped from host to container."
  default     = ""
}

variable "hosted_zone" {
  description = "Hosted zone name to be used"
  default     = ""
}

variable "domain_name_prefix" {
  description = "Prefix to add before the hosted zone name to be used. Set to empty if there is an already created record"
  default     = ""
}

variable "app_autoscaling_metric" {
  description = "Target metric"
  default     = "ECSServiceAverageCPUUtilization"
}

variable "target_tracking_target_value" {
  description = "Target track value"
  default     = 40
}

variable "target_tracking_scale_in_cooldown" {
  description = "Cooldown between scaling in actions (in seconds)"
  default     = 90
}

variable "target_tracking_scale_out_cooldown" {
  description = "Cooldown between scaling out actions (in seconds)"
  default     = 300
}

variable "capacity_provider_strategy" {
  description = "Capacity provider strategy inherited from the target ECS cluster (if needed)"
  default = {
    capacity_provider_name  = "scrapy-cp"
    base                    = 1
    weight                  = 1
  }
}

variable "listener_rule_paths" {
  description = "String list with all paths that the listener rule shall take in account "
  default     = "/hello"
}

variable "listeners_list" {
  description = "List with the ALB listeners where to add rules in. Available ones are: '80', '8080' and '443'"
  type        = list(string)
  default     = ["80"]
}

variable "ecr_repository_name" {
  description = "ECS Repository name. By default is the service name."
  default     = ""
}


variable "service_max_capacity" {
  description = "Maximum number of tasks"
}

variable "service_min_capacity" {
  description = "Minimum number of tasks"
}

variable "target_value" {
  description = "Target track value"
  default = 40
}

variable "metric" {
  description = "Target metric"
  default = "ECSServiceAverageCPUUtilization"
}

variable "scale_in_cooldown" {
  description = "Cooldown between scaling in actions (in seconds)"
  default     = "90"
}
variable "scale_out_cooldown" {
  description = "Cooldown between scaling out actions (in seconds)"
  default     = "300"
}

variable "health_check_grace_period" {
  description = "A grace period (in seconds) "
  default     = 0
}

variable "minimum_healthy_percent" {
  default = 100
}

variable "log_retention_days" {
  description = "The number of days to keep the logs."
  default     = 3
}

variable "alb_container_port" {
  description = "The ALB port to redirect to the container."
  default     = 80
}

variable "port_mapping_protocol" {
  default = "tcp"
}