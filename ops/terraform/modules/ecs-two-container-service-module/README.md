# ECS service module
This folder contains a [Terraform](https://www.terraform.io/) module to deploy an ECS Service in a specified ECS Cluster
that includes a Task definition, ALB Target group, Cloudwatch Log group, ECR repository and a subset of default alarms.

This module is a join of different AWS Services in order to speed up and modularize the services to deploy. 

This is intended to be used from the root of each service project by use of a `terraform.tf` file that calls this 
module and defines all its parameters.


## How to use this module?

This folder defines a [Terraform module](https://www.terraform.io/docs/modules/usage.html). It can be used by importing 
the referenced module; just code by adding a `module` configuration and setting its `source` 
parameter to the URL of this folder:

```hcl-terraform
module "service_name" {
# REQUIRED FIELDS
  source                  = "./modules/ecs-two-container-service-module"
  service_name            = "service_name"
  container_name          = "service_name"
  container_port          = 8080
  ecs_cluster_name        = "demo-cluster"
  alb_name                = "demo-alb"
  alb_port                = 80
  tg_name                 = "service_name-tg"
  health_check_path       = "/status"
  domain_name_prefix      = "service_name"
  hosted_zone             = "hosted_zone_name"
  ecs_log_group_name      = "service_name-log"
  stream_prefix           = "service_name-"
  cpu                     = 2048
  memory                  = 470

# OPTIONAL FIELDS (default values)
  host_port                           = 0  # Set to 0 makes host port being used dynamically (for ECS-ALB)
  ecs_desired_count                   = 1
  ecs_deployment_min_healthy_percent  = 75
  ecs_service_role                    = "arn:aws:iam::917124603846:role/ecsServiceRole"
}
```