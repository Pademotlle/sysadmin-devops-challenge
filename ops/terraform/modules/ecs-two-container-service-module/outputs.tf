output "aws_ecs_service_id" {
  value = aws_ecs_service.service.id
}

output "service1_ecr_repository_url" {
  value = aws_ecr_repository.service1_ecr_repository.repository_url
}

output "service2_ecr_repository_url" {
  value = aws_ecr_repository.service2_ecr_repository.repository_url
}

output "aws_ecs_task_definition_id" {
  value = aws_ecs_task_definition.task_definition.id
}