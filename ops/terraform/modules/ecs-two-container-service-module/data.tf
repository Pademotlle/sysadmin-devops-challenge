data aws_alb "load_balancer" {
  name = var.alb_name
}

data aws_alb_listener "listener" {
  load_balancer_arn = data.aws_alb.load_balancer.arn
  port              = 80
}

data aws_iam_role "ecs_service_role" {
  name = "AWSServiceRoleForECS"
}

/* Container definitions */
data "template_file" "container_definition" {
  template                  = file("${path.module}/tasks-definitions/task-definition.json")
  vars = {
    region                  = var.region
    protocol                = var.port_mapping_protocol
    service1_image_url      = format("%s:latest", aws_ecr_repository.service1_ecr_repository.repository_url)
    service1_container_name = var.service1_container_name
    service1_log_group      = var.service1_ecs_log_group
    service1_stream_prefix  = var.service1_stream_prefix
    service1_cpu            = var.service1_cpu
    service1_memory         = var.service1_memory
    service1_container_port = var.service1_container_port
    service1_host_port      = var.host_port
    service2_image_url      = format("%s:latest", aws_ecr_repository.service2_ecr_repository.repository_url)
    service2_container_name = var.service2_container_name
    service2_log_group      = var.service2_ecs_log_group
    service2_stream_prefix  = var.service2_stream_prefix
    service2_cpu            = var.service2_cpu
    service2_memory         = var.service2_memory
    service2_container_port = var.service2_container_port
    service2_host_port      = var.host_port
  }
}
