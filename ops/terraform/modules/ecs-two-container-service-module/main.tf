/*
 * ECS Service Task definition
 */
resource "aws_ecs_task_definition" "task_definition" {
  family                = var.service_name
  container_definitions = data.template_file.container_definition.rendered
//  network_mode          = "host"
}

/*
 * Target group that forwards traffic to the ECS service from the ALB
 */
resource "aws_alb_target_group" "ecs_service_tg" {
  name        = format("%s-tg", var.service_name)
  port        = var.alb_port
  protocol    = var.alb_tg_protocol
  vpc_id      = var.vpc_id
  target_type = var.alb_tg_target_type

  health_check {
    path    = var.health_check_path
    matcher = var.healthy_status_code
  }
}

/*
 * ECS Service
 */
resource "aws_ecs_service" "service" {
  name                               = var.service_name
  cluster                            = var.ecs_cluster_name
  task_definition                    = aws_ecs_task_definition.task_definition.id
  desired_count                      = var.ecs_desired_count
  iam_role                           = data.aws_iam_role.ecs_service_role.arn
  deployment_minimum_healthy_percent = var.minimum_healthy_percent
  health_check_grace_period_seconds  = var.health_check_grace_period

  lifecycle {
    ignore_changes = [
      capacity_provider_strategy
    ]
  }

  dynamic "capacity_provider_strategy" {
    for_each = var.capacity_provider_strategy
    content {
      capacity_provider = capacity_provider_strategy.value.capacity_provider_name
      weight            = capacity_provider_strategy.value.weight
      base              = capacity_provider_strategy.value.base

     }
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.ecs_service_tg.arn
    container_name   = var.service2_container_name
    container_port   = var.service2_container_port
  }
}

/*
 * Cloudwatch log group
 */
resource "aws_cloudwatch_log_group" "service1_log_group" {
  name              = var.service1_ecs_log_group
}

resource "aws_cloudwatch_log_group" "service2_log_group" {
  name              = var.service2_ecs_log_group
}

/*
 * ECR repository
 */
resource "aws_ecr_repository" "service1_ecr_repository" {
  name = var.service1_container_name
}

resource "aws_ecr_repository" "service2_ecr_repository" {
  name = var.service2_container_name
}

/*
 * Listener rule in the target ALB that delivers traffic to the desired target group
 */
resource "aws_alb_listener_rule" "alb_listener_rule" {
  count = contains(var.listeners_list, "80") ? 1 : 0
  listener_arn        = data.aws_alb_listener.listener.arn

  action {
    type              = var.alb_listener_rule_action_type
    target_group_arn  = aws_alb_target_group.ecs_service_tg.arn
  }

  condition {
    path_pattern {
      values = split(",", var.listener_rule_paths)
    }
  }
}