resource "aws_alb" "load_balancer" {
  name            = var.elb_name
  internal        = true
  security_groups = var.security_groups_ids
  subnets         = var.subnets_ids
  idle_timeout    = var.idle_timeout

  tags = {
    Name = var.elb_name
  }
}

resource "aws_alb_listener" "listener" {
  for_each = var.listeners
  load_balancer_arn = aws_alb.load_balancer.arn
  port              = lookup(each.value, "port")
  protocol          = lookup(each.value, "protocol")

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "application/json"
      message_body = "{\"status\":\"fail\"}"
      status_code  = "404"
    }
  }
}