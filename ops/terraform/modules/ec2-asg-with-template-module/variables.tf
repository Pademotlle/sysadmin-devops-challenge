variable "region" {
  description = "The AWS region to create resources in."
}

variable "launch_template_name" {
  description = "The name of the Launch Configuration."
}

variable "image_id" {
  description = "The id of the AMI to use in the cluster."
}

variable "instance_type" {
  description = "The type of image to use on the cluster."
}

variable "security_groups_ids" {
  description = "The security groups to assign to the instances."
}

variable "autoscaling_group_name" {
  description = "The name of the security group."
}

variable "availability_zones" {
  description = "The availability zones for the autoscaling group to create instances in."
}

variable "subnets_ids" {
  description = "The list of subnets."
}

variable "min_size" {
  description = "The minimum size for the autoscaling group."
}

variable "max_size" {
  description = "The maximum size for the autoscaling group."
}

variable "desired_size" {
  description = "The desired size for the autoscaling group."
}

variable "iam_instance_role" {
  description = "The attribute name of the IAM instance profile to associate with launched instances."
  default     = "ecsInstanceRole"
}

variable "protection_from_scale_in_status" {
  description = "Allows setting instance protection. The autoscaling group will not select instances with this setting for termination during scale in events."
}

variable "asg_wait_for_capacity_timeout" {
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out."
  default     = "4m"
}

variable "asg_health_check_grace_period" {
  description = "Time (in seconds) after instance comes into service before checking health."
  default     = 180
}

variable "asg_launch_template_path" {
  description = "Relative path where AutoScaling Group launch template is."
  default     = "launch-configuration-scripts/asg-launch-config.sh"
}

variable "ecs_cluster_name" {
  description = "ECS cluster name"
}

variable "ecs_ec2_policies" {
  description = "Policies that grants permissions to EC2 ASG to interact with ECS"
  default     = [
    "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role",
    "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole",
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess",
    "arn:aws:iam::aws:policy/AWSCodeDeployRoleForECS",
    "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole",
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy",
    "arn:aws:iam::aws:policy/ElasticLoadBalancingFullAccess",
    "arn:aws:iam::aws:policy/AmazonECS_FullAccess",
  ]
}