resource "aws_autoscaling_group" "ec2_autoscaling_group" {
  name                      = var.autoscaling_group_name
  vpc_zone_identifier       = var.subnets_ids
  min_size                  = var.min_size
  max_size                  = var.max_size
  desired_capacity          = var.desired_size
  protect_from_scale_in     = var.protection_from_scale_in_status
  health_check_grace_period = var.asg_health_check_grace_period
  wait_for_capacity_timeout = var.asg_wait_for_capacity_timeout
  launch_template {
    id      = aws_launch_template.autoscaling_launch_template.id
    version = "$Latest"
  }

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = var.autoscaling_group_name
    propagate_at_launch = true
  }
}

resource "aws_launch_template" "autoscaling_launch_template" {
  name_prefix             = var.launch_template_name
  image_id                = var.image_id
  instance_type           = var.instance_type
  user_data               = base64encode(data.template_file.launch_config.rendered)
  vpc_security_group_ids  = var.security_groups_ids
  iam_instance_profile {
    name = aws_iam_instance_profile.ec2_instance_profile.name
  }
}
