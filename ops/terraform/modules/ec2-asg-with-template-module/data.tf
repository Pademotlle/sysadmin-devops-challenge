data template_file "launch_config" {
  template  = file("${path.module}/launch-configuration-scripts/asg-launch-config.sh")
  vars      = {
	  cluster_name = var.ecs_cluster_name
  }
}