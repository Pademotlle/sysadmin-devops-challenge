resource "aws_iam_role" "ecs_instance_role" {
  name = "ecsInstanceRole"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  count      = length(var.ecs_ec2_policies)
  role       = aws_iam_role.ecs_instance_role.name
  policy_arn = var.ecs_ec2_policies[count.index]
}

resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "ecsInstanceProfileRole"
  role = aws_iam_role.ecs_instance_role.name
}