# EC2 Autoscaling group and capacity providers module
This folder contains a [Terraform](https://www.terraform.io/) module that defines an `AutoScaling Group` for EC2/ECS 
service with a launch template.

To use this module is important to have `iam:CreateRole` permission

---
### How to use this module?
This folder defines a [Terraform module](https://www.terraform.io/docs/modules/usage.html). It can be used by importing 
the referenced module; just code by adding a `module` configuration and setting its `source`.

```hcl-terraform
module "ecs_autoscaling_group" {
  # REQUIRED FIELDS
  source = ""
  region = "ue-west-1"
  service_name = "service_name"
  image_id = "ami-xxxxxxx"
  instance_type = "t3.nano"
  key_name = "key"
  autoscaling_group_name = "service_name-asg"
  security_groups_ids = "sg-xxxxxxx"
  availability_zones = "xx-xxxx-xx,yy-yyyy-yy,zz-zzzz-zz"
  subnets_ids = "subnet-xxxxxxx"
  min_size = 1
  max_size = 3
  desired_size = 1
  volume_size = 30
  launch_template_name = "service_name-template"
}
``` 

### Launch template
In order to define an Autoscaling Group, is necessary to specify its Launch Template.
For this reason it's necessary to create a source data that specify where is placed the Launch Configuration Template 
file and the target Cluster.

```hcl-terraform
data "template_file" "launch_template" {
  template  = file("${path.module}/launch-configuration-scripts/asg-launch-config.sh")
  vars      = {
    cluster_name = "ecs_cluster_name"
  }
}
```