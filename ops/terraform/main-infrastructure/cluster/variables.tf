variable "aws_region" {
  description = "AWS region to operate"
  default     = "eu-west-1"
}

variable "service_name" {
  description = "ECS cluster name"
  default     = "challenge"
}

variable "vpc_cidr_block" {
  description = "default-vpc cidr block"
  default     = "172.31.0.0/16"
}

variable "availability_zones" {
  description = "The availability zones for the autoscaling group to create instances in."
  default     = "eu-west-1a"
}

variable "subnet_1_name" {
  description = "VPC subnet name"
  default     = "challenge-subnet-1"
}

variable "subnet_2_name" {
  description = "VPC subnet name"
  default     = "challenge-subnet-2"
}

variable "subnet_3_name" {
  description = "VPC subnet name"
  default     = "challenge-subnet-3"
}

variable "security_group_name" {
  description = "Security group name"
  default     = "challenge-sg"
}

variable "cp_managed_termination_protection" {
  description = "Enables or disables container-aware termination of instances in the auto scaling group when scale-in happens"
  default     = "ENABLED"
}

variable "cp_max_scaling_step_size" {
  description = "The maximum step adjustment size. A number between 1 and 10,000. "
  default     = 5
}

variable "cp_step_size" {
  description = "Number of instances that capacity provider may raise up. Limited by the max_scaling_step_size."
  default     = 5
}

variable "cp_min_scaling_step_size" {
  description = "The minimum step adjustment size. A number between 1 and 10,000."
  default     = 1
}

variable "cp_status" {
  description = "Whether auto scaling is managed by ECS. Valid values are ENABLED and DISABLED"
  default     = "ENABLED"
}

variable "cp_target_capacity" {
  description = "The target utilization for the capacity provider. A number between 1 and 100."
  default     = 100
}

variable "ecs_optimized_amis" {
  default = {
    us-east-1      = "ami-05250bd90f5750ed7"
    us-east-2      = "ami-078d79190068a1b35"
    us-west-1      = "ami-0667a9cc6a93f50fe"
    us-west-2      = "ami-06cb61a83c506fe88"
    eu-west-1      = "ami-0c4b9f15bc61a5ba8"
    eu-west-2      = "ami-0e2ab4ec1609a6006"
    eu-central-1   = "ami-00364a85f9634c4f3"
    ap-northeast-1 = "ami-0ca6d4eb36c5aae78"
    ap-southeast-1 = "ami-0167e83338bdf98c2"
    ap-southeast-2 = "ami-095016ddc8e84b54e"
    ca-central-1   = "ami-0761de529c3d697c5"
  }
}

variable "instance_type" {
  description = "The type of image to use on the cluster."
  default     = "t3.micro"
}

variable "asg_min_size" {
  description = "ECS Autoscaling group minimum size"
  default     = 1
}

variable "asg_max_size" {
  description = "ECS Autoscaling group maximum size"
  default     = 1
}

variable "asg_desired_size" {
  description = "ECS Autoscaling group desired capacity. EC2 instances num that should be running."
  default     = 1
}

variable "asg_protection_from_scale_in" {
  description = "Allows setting instance protection. The autoscaling group will not select instances with this setting for termination during scale in events."
  default     = true
}

variable "protocol" {
  description = "Listener protocol"
  default     = "HTTP"
}

variable "listeners" {
  description = "Map/s with all listeners to add. Port and Protocol shall be specified"
  default = {
    http = {
      port: 80,
      protocol: "HTTP"
    }
  }
}