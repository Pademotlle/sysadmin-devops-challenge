module "challenge_asg" {
    source                                          = "../../modules/ec2-asg-with-template-module"
    region                                          = var.aws_region
    ecs_cluster_name                                = format("%s-cluster", var.service_name)
    autoscaling_group_name                          = format("%s-asg", var.service_name)
    launch_template_name                            = format("%s-asg-launch-template", var.service_name)
    image_id                                        = var.ecs_optimized_amis[var.aws_region]
    instance_type                                   = var.instance_type
    security_groups_ids                             = [data.aws_security_group.secure-group.id]
    availability_zones                              = var.availability_zones
    subnets_ids                                     = data.aws_subnet_ids.public_subnets.ids
    min_size                                        = var.asg_min_size
    max_size                                        = var.asg_max_size
    desired_size                                    = var.asg_desired_size
    protection_from_scale_in_status                 = var.asg_protection_from_scale_in
}