data aws_vpc "challenge-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name   = "challenge-vpc"
  }
}

data "aws_subnet_ids" "public_subnets" {
  vpc_id = data.aws_vpc.challenge-vpc.id
}

data "aws_security_group" "secure-group" {
  name = var.security_group_name
}