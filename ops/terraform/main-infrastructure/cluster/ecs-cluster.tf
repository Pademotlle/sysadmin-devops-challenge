resource "aws_ecs_cluster" "challenge_cluster" {
  name = format("%s-cluster", var.service_name)
  capacity_providers = [aws_ecs_capacity_provider.challenge-cluster-capacity-provider.name]
}