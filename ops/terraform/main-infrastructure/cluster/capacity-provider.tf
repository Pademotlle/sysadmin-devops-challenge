resource "aws_ecs_capacity_provider" "challenge-cluster-capacity-provider" {
  name = format("%s-cp", var.service_name)
  depends_on = [aws_iam_service_linked_role.ecs]
  auto_scaling_group_provider {
    auto_scaling_group_arn          = module.challenge_asg.autoscaling_group_arn
    managed_termination_protection  = var.cp_managed_termination_protection

    managed_scaling {
      maximum_scaling_step_size = var.cp_max_scaling_step_size
      minimum_scaling_step_size = var.cp_min_scaling_step_size
      status                    = var.cp_status
      target_capacity           = var.cp_target_capacity
    }
  }
}

resource "aws_iam_service_linked_role" "ecs" {
  aws_service_name = "ecs.amazonaws.com"
}
