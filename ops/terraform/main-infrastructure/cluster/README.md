# (SysAdmin and Devops) Challenge Cluster
This folder contains the [Terraform](https://www.terraform.io/) structure that creates and manages the main 
infrastructure that will hold the that will be used to hold the microservice that contains a reverse-proxy server and a 
web app.

This infrastructure is composed by next AWS services:

  * AWS ECS cluster
  * AWS ECS service
  * AWS ECS capacity providers  
  * AWS ECR
  * AWS EC2 Autoscaling Group
  * AWS Application Load Balancer
  * AWS Target group
  * AWS Cloudwatch log group