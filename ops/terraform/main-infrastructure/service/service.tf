module "nginx-and-app-service" {
  source                        = "../../modules/ecs-two-container-service-module"
  alb_name                      = var.alb_name
  service1_container_name       = var.web_app_container_name
  service2_container_name       = var.nginx_container_name
  service1_container_port       = var.web_app_container_port
  service2_container_port       = var.nginx_container_port
  service1_cpu                  = var.web_app_cpu
  service2_cpu                  = var.nginx_cpu
  ecs_cluster_name              = var.ecs_cluster_name
  service1_ecs_log_group        = format("%s-log", var.web_app_container_name)
  service2_ecs_log_group        = format("%s-log", var.nginx_container_name)
  health_check_path             = var.health_check_path
  service1_memory               = var.web_app_memory
  service2_memory               = var.nginx_memory
  service_max_capacity          = var.service_max_capacity
  service_min_capacity          = var.service_min_capacity
  service_name                  = var.service_name
  service1_stream_prefix        = format("%s-", var.web_app_container_name)
  service2_stream_prefix        = format("%s-", var.nginx_container_name)
  tg_name                       = format("%s-tg", var.service_name)
  vpc_id                        = data.aws_vpc.challenge-vpc.id
  capacity_provider_strategy    = var.capacity_provider_strategy
  ecs_desired_count             = var.ecs_desired_count
  minimum_healthy_percent       = var.minimum_healthy_percent
}