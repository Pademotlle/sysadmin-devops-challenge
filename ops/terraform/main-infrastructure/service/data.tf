data aws_vpc "challenge-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name   = "challenge-vpc"
  }
}