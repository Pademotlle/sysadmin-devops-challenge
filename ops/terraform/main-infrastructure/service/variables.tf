variable "service_name" {
  description = "Service project name"
  default     = "microservice"
}

variable "aws_region" {
  description = "AWS region to operate"
  default     = "eu-west-1"
}

variable "vpc_cidr_block" {
  description = "vpc cidr block"
  default     = "172.31.0.0/16"
}

variable "ecs_cluster_name" {
  description = "The name of the Amazon ECS cluster."
  default     = "challenge-cluster"
}

variable "web_app_container_name" {
  description = "The container's name."
  default     = "web-app"
}

variable "web_app_container_port" {
  description = "Container service exposed port"
  default     = 8080
}

variable "nginx_container_name" {
  description = "The container's name."
  default     = "nginx"
}

variable "nginx_container_port" {
  description = "Container service exposed port"
  default     = 80
}

variable "ecs_desired_count" {
  description = "ECS service desired count"
  default     = 1
}

variable "ecs_deployment_min_healthy_percent" {
  description = "ECS service Deployment Minimum Healthy percent value"
  default     = 75
}

variable "alb_name" {
  description = "Application Load Balancer name"
  default     = "challenge-alb"
}

variable "alb_tg_target_type" {
  description = "Type of target for target group"
  default     = "instance"
}

variable "alb_tg_protocol" {
  description = "Type of target for target group"
  default     = "HTTP"
}

variable "alb_listener_rule_action_type" {
  description = "The type of routing action."
  default     = "forward"
}

variable "healthy_status_code" {
  description = "Expected status code for the healthcheck"
  default     = 200
}

variable "health_check_path" {
  description = "Health check path for ALB TG. i.e.: /status"
  default     = "/hello"
}

variable "web_app_cpu" {
  description = "The number of cpu units used by the task"
  default     = 1020
}

variable "web_app_memory" {
  description = "The amount (in MiB) of memory used by the task."
  default     = 400
}

variable "nginx_cpu" {
  description = "The number of cpu units used by the task"
  default     = 1020
}

variable "nginx_memory" {
  description = "The amount (in MiB) of memory used by the task."
  default     = 400
}

variable "capacity_provider_strategy" {
  description = "Capacity provider strategy inherited from the target ECS cluster (if needed)"
  default = [
    {
      capacity_provider_name  = "challenge-cp"
      base                    = 1
      weight                  = 1
    }
  ]
}

variable "listener_rule_paths" {
  description = "String list with all paths that the listener rule shall take in account "
  default     = ""
}

variable "listeners_list" {
  description = "List with the ALB listeners where to add rules in. Available ones are: '80', '8080' and '443'"
  type        = list(string)
  default     = ["80"]
}

variable "web_app_ecr_repository_name" {
  description = "ECS Repository name."
  default     = "challenge/web-app"
}

variable "nginx_ecr_repository_name" {
  description = "ECS Repository name."
  default     = "challenge/nginx"
}

variable "service_max_capacity" {
  description = "Maximum number of tasks"
  default     = 1
}

variable "service_min_capacity" {
  description = "Minimum number of tasks"
  default     = 1
}

variable "target_value" {
  description = "Target track value"
  default = 40
}

variable "health_check_grace_period" {
  description = "A grace period (in seconds) "
  default     = 0
}

variable "minimum_healthy_percent" {
  default = 100
}

variable "log_retention_days" {
  description = "The number of days to keep the logs."
  default     = 3
}