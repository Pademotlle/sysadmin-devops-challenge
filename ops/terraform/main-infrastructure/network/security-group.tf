resource "aws_security_group" "secure-group" {
  name        = "challenge-sg"
  description = "Security group for web that allows web traffic from internet to inside"
  vpc_id      = aws_vpc.challenge-vpc.id

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = [jsondecode(data.aws_secretsmanager_secret_version.account_secret_ver.secret_string)[var.office_address]]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # cidr_blocks = [jsondecode(data.aws_secretsmanager_secret_version.account_secret_ver.secret_string)[var.office_address]]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    # cidr_blocks = [jsondecode(data.aws_secretsmanager_secret_version.account_secret_ver.secret_string)[var.office_address]]
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "secure-group"
  }
}