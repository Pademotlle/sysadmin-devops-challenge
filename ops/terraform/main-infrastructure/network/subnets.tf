resource "aws_subnet" "challenge-subnet-1" {
  vpc_id                  = aws_vpc.challenge-vpc.id
  cidr_block              = cidrsubnet(aws_vpc.challenge-vpc.cidr_block, 4, 1)
  map_public_ip_on_launch = "true"
  availability_zone       = var.aws_az_a
  depends_on              = [aws_vpc.challenge-vpc]
  tags = {
    Name = var.subnet_1_name
  }
}
resource "aws_subnet" "challenge-subnet-2" {
  vpc_id                  = aws_vpc.challenge-vpc.id
  cidr_block              = cidrsubnet(aws_vpc.challenge-vpc.cidr_block, 4, 2)
  map_public_ip_on_launch = "true"
  availability_zone       = var.aws_az_b
  depends_on              = [aws_vpc.challenge-vpc]
  tags = {
    Name = var.subnet_2_name
  }
}

resource "aws_subnet" "challenge-subnet-3" {
  vpc_id                  = aws_vpc.challenge-vpc.id
  cidr_block              = cidrsubnet(aws_vpc.challenge-vpc.cidr_block, 4, 3)
  map_public_ip_on_launch = "true"
  availability_zone       = var.aws_az_c
  depends_on              = [aws_vpc.challenge-vpc]
  tags = {
    Name = var.subnet_3_name
  }
}