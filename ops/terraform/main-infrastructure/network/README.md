# Sysadmin and Devops Challenge Project Network

---
### VPC
Public vpc by default to handle the Devops challenge infrastructure.

---
### Public subnets
Public subnets (to handle the 3 AZs) by default to handle the Devops challenge infrastructure.

---
### Security group
Security group to give access to the required people. 
In the code, the ingress cidr blocks are set to `0.0.0.0/0` because I thought that it's your desire to test this 
infrastructure, it would be easier. By the way, I would set the accepted IP addresses using a secret or something like 
that. 