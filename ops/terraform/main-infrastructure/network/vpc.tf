resource "aws_vpc" "challenge-vpc" {
  cidr_block            = var.vpc_cidr
  enable_dns_hostnames  = true
  tags                  = {
    Name = "challenge-vpc"
  }
}
