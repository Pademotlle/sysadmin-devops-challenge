"""
This is the main module for the Web Application module
"""

import logging

from flask import Flask, jsonify

app = Flask(__name__)
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


@app.route("/hello", methods=["GET"])
def worldwide_greetings():
    """ Route for '/hello' endpoint. """

    logger.info("Greeting request received!")

    return jsonify(hello="world"), 200


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
