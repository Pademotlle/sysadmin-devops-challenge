"""
This module contains the unitary testing code needed to check the Flask web app basic performance
"""
import json
import logging

from unittest import mock, TestCase
from web_app.app import app


class TestApp(TestCase):
    """ Flask Web App unit test class """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp
        """

        cls.valid_status = 200
        cls.default_hello_data = {'hello': 'world'}

    def setUp(self) -> None:
        """ Test setUp """

        self.testing_app = app.test_client()
        self.testing_app.testing = True

    def tearDown(self) -> None:
        """ Test tearDown """

        pass

    def test_worldwide_greetings(self) -> None:
        """ This method unittests the `worldwide_greetings` method in its nominal scenario. """

        result = self.testing_app.get("/hello")
        self.assertEqual(result.status_code, self.valid_status)
        self.assertEqual(json.loads(result.data), self.default_hello_data)
