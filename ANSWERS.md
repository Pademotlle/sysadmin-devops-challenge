## How long did it take to finish the test?
Well I had more or less one week to carry out this solution, but I had a very busy week in my current workplace and
in the weekend I had to stay with my nephew, so I had few hours to work in, so I had to overnight some days.

---
## If you had more time, how could you improve your solution?
* I would fix the issue with the non registered EC2 ASG instances into the ECS cluster.
* If I would had infinite time, I would use Kubernetes instead of all ECS stuff. In case I had just some more time...
* I would improve some security issues (I think that right now the VPC is public for example)
* I would use S3 Buckets to store the TFstates into it. But as we don't share the same AWS account, it would become an 
  issue.
* I would implement a way to automate the infrastructure destruction (sorry for that, lack of time).
* Improve logging and metrics retribution.  
* I would add some Cloudwatch alarms in the service
* I would create a hosted zone and an alias bound to the ALB (also I didn't wanted to make you to increase your billing 
  with the creation of a new hosted zone in case you want to test it)
* I would add CI to the web app
* I would create integration testing for the app
* I would use only the needed IAM roles, I had no time to check all of them so I had some issues with them.

---
## What changes would you do to your solution to be deployed in a production environment?
First of all, I used ECS to place the production in a clusterized infrastructure but I want to say that I would prefer 
so much using Kubernetes, working with ECS it's a pain the ass. I have experience with it and have more knowledge, but 
I really hate it (IAM roles, AWS client interface, the need of interaction with other AWS services, very big large etc.) 
Why did I not use Kubernetes? Well, it's true that I know the bases and I did something at home but if I want to show 
my knowledge and my `current` skills, I need to do my best with my best. In previous job offer tests I tried to show what 
can I do with Kubernetes, but as I have no real experience with it, those companies prefered to choose other candidates.

---
## Why did you choose this language over others?
Because Python is the one I have more experience with. It's very easy to use, to implement, to run and Flask framework 
is very easy to use.


---
## What was the most challenging part and why?
To be honest, the `Using one single command we want to deploy all the required infrastructure`. Too many time is needed 
to create that (at least with ECS, probably with K8 would be faster, but it still needs some time). 