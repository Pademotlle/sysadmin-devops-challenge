# SysAdmin DevOps challenge
Deploy a new microservice to production that given a GET request to the /hello endpoint it will return a JSON response.

---
### How to
This microservice can be used from both staging or production environments. The staging one will deploy a two docker
service inside the own hosted Docker engine and the production one will deploy the whole needed infrastructure to 
place the microservice into AWS cloud provider.

In order to deploy the service it's only needed to play a single Ansible playbook:
```shell
ansible-playbook ./ops/ansible/deploy-playbook-yml
```

If any variable is defined, it will be deployed into staging (locally), in case it's desired to deploy in production 
just enter:
```shell
ansible-playbook ./ops/ansible/deploy-playbook-yml --extra-vars "env=production"
```

---
### Software requirements
In order to use this project, will be needed the software requirements listed below:
* AWS Client insalled (sudo apt-get install awscli)
* Have own AWS credentials store account inside `~/.aws/credentials`
* Terraform already installed (> 0.12v)
* Ansible already installed (>2.0v)
* Have enough IAM role permissions to create IAM role permissions in the own user

---
### Why did I choose this solution?  
Basically because is the best way I think I can develop this solution with the given time.
AWS ECS, Terraform, Docker and Ansible are platforms I have enough experience to develop this whole infrastructure
in only one week (even if I work full time in my current company and my personal duties).

I really think it's not the best solution, I would prefer using Kubernetes before ECS for example, but are the 
platforms I have deeper knowledge.


---
### Problems
I had problems with the EC2 AutoscalingGroup that was not registering instances into the ECS Cluster. I remember that I 
had issues with this in the past and I tried to fix with many things, but in conclusion: I've been unable to fix this.
I'm very sure that the issue remains on the IAM Roles, but being honest: I don't know which one is missing.
